# Cahier des charges

## Sujet

Reproduction du site "Programming Challenges" pour le langage C#.

## But

Créer un site web proposant différents "challenges", consistant à apprendre à programmer d'une manière "propre"tout en respectant des tests unitaires donnés par un énoncé précis. 

Le site web permet de se connecter/inscrire, de commencer un challenge et d'accéder à son énoncé, ainsi que la possibilité d'upload le code source de l'utilisateur, de le compiler et de le valider (selon les tests unitaires/énoncé). Il est alors évaluer et noter en fonction du resultat des test unitaire ainsi que du temps d'exécution de la compilation du projet.

## Spécifications

- Soumission de source (C# console) à compiler côté serveur
  - Retour pour l'utilisateur (fail, ok, etc...)
- Éxécution de tests unitaires
- Classement selon la durée d'éxécution

## Restrictions
- Aucune compatibilité ne sera assurée avec Internet Explorer.
- Pour le moment, l'application ne pourra que compilé du langage C# (possibilité d'évolution)
- Le projet est disponible uniquement via le container Proxmox


## Environnement
###Langages de programmations
- Laravel 5.5 (Framework PHP)
- Materializecss (Framework CSS)
### Documentation
- Github
- Markdown
- Latex

### Système d'éxploitation / serveurs

- Proxmox
  - Linux container (debian)
- Développement sur Windows / Mac

## Livrables

- Fichier zip contenant le projet Laravel
- Documentation et poster au format PDF
- Planning sous format excel