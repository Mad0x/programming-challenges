<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nickname', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Return the challenges created by the user
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function createdChallenges()
    {
        return $this->hasMany('App\Challenge');
    }

    /**
     * Return the challenges of the user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function challenges()
    {
        return $this->belongsToMany('App\Challenge')->withPivot('executionTime','status')->withTimestamps();
    }
}
