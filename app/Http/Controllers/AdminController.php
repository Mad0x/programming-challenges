<?php

namespace App\Http\Controllers;

use App\Challenge;

use App\Http\Requests\HandleChallengeRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class AdminController extends Controller
{
    /**
     * Return a view containing all the challenges
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $challenges = Challenge::all();
        return view('admin.admin', compact('challenges'));
    }

    /**
     * Delete the challenge
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteChallenge($id)
    {
        $challenge = Challenge::findOrFail($id);
        $challenge->delete();

        return redirect('/admin')->with('success', 'Challenge <strong>'. $challenge->name .'</strong> successfully deleted !');
    }

    /**
     * View for edit a challenge
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editViewChallenge($id)
    {
        $challenge = Challenge::findOrFail($id);
        return view('admin.editChallenge', compact('challenge'));
    }

    /**
     * Update the challenge in the DB
     * @param HandleChallengeRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function editChallenge(HandleChallengeRequest $request)
    {

        $challenge = Challenge::findOrFail($request->id);
        $challenge->description = $request->description;
        $challenge->testDescription = $request->unitTest;
        $challenge->difficulty = $request->difficulty;
        if($request->output != "")
        {
            Storage::disk('public')->put('challenges/'.$challenge->name.'/output.txt',$request->output);

        }
        $challenge->save();

        return redirect('/admin')->with('success', 'Challenge <strong>'. $challenge->name .'</strong> successfully edited !');
    }
}
