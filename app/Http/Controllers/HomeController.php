<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $challenges = Auth::user()->challenges;
        return view('home', compact('challenges'));
    }

    /**
     *  Save the profile of the user
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function save(Request $request)
    {

        $user = Auth::user();

        //If the nickname is not equal to the old nickname
        if($request->input('nickname') != $user->nickname)
        {
            $this->validate($request, ['nickname' => 'required|string|max:255']);
        }

        //If the mail is not equal to the old mail
        if($request->input('email') != $user->email)
        {
            $this->validate($request, ['email' => 'required|string|email|max:255|unique:users']);
        }

        //If the password is not empty
        if($request->input('password') != "")
        {
            $this->validate($request, [
                'password' => 'required|string|min:6|confirmed',
            ]);
            $user->password = bcrypt($request->input('password'));
        }

        $user->nickname = $request->input('nickname');
        $user->email = $request->input('email');
        $user->save();

        return redirect()->back()->with('success', 'Profile updated!');
    }

}
