<?php

namespace App\Http\Controllers;

use App\Challenge;

use App\Http\Requests\HandleChallengeCompilationRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessTimedOutException;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Config;

class ChallengeController extends Controller
{
    /**
     * Return a view containing all the challenges
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showAll()
    {
        $challenges = Challenge::all();
        return view('challenges.index', compact('challenges'));
    }

    /**
     * Return a view containing a specific challenge
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showOne($id)
    {
        $challenge = Challenge::findOrFail($id);
        return view('challenges.challenge', compact('challenge'));
    }


    /**
     * Compilation function, handling all the compilation work when the user post his code
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function compile(HandleChallengeCompilationRequest $request)
    {
        $challenge = Challenge::findOrFail($request->id);
        $userPath = Config::get('constants.compilation.DEFAULT_PATH') . Auth::user()->email;
        $challengePath = $userPath . '/' . $challenge->name;
        $participate = Auth::user()->challenges()->where('challenge_id', $challenge->id)->first();

        //If the user don't participate to the challenge yet, copy the directory of the challenge
        if ($participate == null) {
            $participate = $this->copyChallenge($challenge, $userPath);
        }

        //If the user didn't uploaded any file...
        if ($request->file == null) {
            Storage::disk('public')->put('attendees/' . Auth::user()->email . '/' . $challenge->name . '/Program.cs', $request->code);
        } else {

            Storage::disk('public')->putFileAs('attendees/' . Auth::user()->email . '/' . $challenge->name . '/',$request->file('file'),'Program.cs');
        }

        //List of processes
        $processes = array(
            'compile' => "mcs -out:$challengePath/$challenge->name.exe $challengePath/Program.cs",
            'execute' => "exec mono $challengePath/$challenge->name.exe",
            'results' => "/usr/bin/time -p -o $challengePath/time.txt -f \"%e\" mono $challengePath/$challenge->name.exe > $challengePath/output.txt",
            'executionTime' => "cat $challengePath/time.txt",
            'diff' => "diff  -w $challenge->outputPath $challengePath/output.txt"
        );

        //Executing all the processes
        $array = $this->execute($processes);

        //If there is an error on compilation
        if ($array['compile'] != "") {
            return back()->with('error', $array['compile']);
        }

        //If there is an error on execution (infinite loop)
        if ($array['execute'] == Config::get('constants.compilation.DEFAULT_LOOP_ERROR')) {
            return back()->with('error', $array['execute']);

        }

        //Check if the outputs are the same
        if ($array['diff'] == "") {
            //Clean the output timetop
            $execution_time = trim($array['executionTime'], "\n");

            //Update the pivot table
            $participate->pivot->executionTime = $execution_time;
            $participate->pivot->status = "FINISHED";
            $participate->pivot->save();
            return back()->with('success', "Congratulations, you achieved the challenge with an execution time of $execution_time [s]");
        } else {
            $participate->pivot->status = "IN PROGRESS";
            $participate->pivot->countFails++;
            $participate->pivot->save();

            return back()->with('error', "Presentation error");
        }
    }


    /***
     * Copy the challenge directory to the user directory
     * @param $challenge
     * @param $userPath
     */
    public function copyChallenge($challenge, $userPath)
    {
        Auth::user()->challenges()->attach($challenge->id);
        $participate = Auth::user()->challenges()->where('challenge_id', $challenge->id)->first();
        $process = new Process("cp -r $challenge->path $userPath");
        $process->run();

        return $participate;
    }

    /***
     * Execute all the processes given
     * @param $processes
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function execute($processes)
    {
        $responseArray = array();
        foreach ($processes as $key => $process) {
            $p = new Process($process);
            $p->setTimeout(Config::get('constants.compilation.DEFAULT_TIME_OUT'));
            try {
                $p->run();
            } catch (ProcessTimedOutException $exception) {
                $p->stop();
                return $responseArray = array_add($responseArray, $key, Config::get('constants.compilation.DEFAULT_LOOP_ERROR'));
            }
            $responseArray = array_add($responseArray, $key, $p->getOutput());
        }
        return $responseArray;
    }

}