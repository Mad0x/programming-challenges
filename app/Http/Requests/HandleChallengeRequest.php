<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class HandleChallengeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Validation rules for the edition of a challenge
     * @return array
     */
    public function rules()
    {
        return [
            'unitTest' => 'required|string',
            'description' => 'required|string',
            'difficulty' => 'required|integer|between:1,10',
        ];
    }
}
