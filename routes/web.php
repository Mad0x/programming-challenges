<?php
use App\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::pattern('id', '\d+');

//Handle all the routes of the challenge controller
Route::group(['middleware' => ['auth'], 'prefix' => 'challenges'], function() {
    Route::get('/', 'ChallengeController@showAll')->name('challenges');
    Route::get('{id}', 'ChallengeController@showOne')->name('challenge');
    Route::post('/compile', 'ChallengeController@compile')->name('compile');
});

Route::get('/', 'HomeController@index')->name('home');
Route::post('save', 'HomeController@save')->name('save');
Auth::routes();

Route::group(['middleware' => ['auth','admin'], 'prefix' => 'admin'], function() {
    Route::get('/', 'AdminController@index')->name('admin');
    Route::get('/delete/{id}', 'AdminController@deleteChallenge') -> name('delete');
    Route::get('/edit/{id}', 'AdminController@editViewChallenge') -> name('editChallenge');
    Route::post('/edit/{id}', 'AdminController@editChallenge') -> name('saveChallenge');
});