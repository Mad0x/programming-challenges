<?php

return [
    'compilation' => [
        'DEFAULT_TIME_OUT' => '5',
        'SIGKILL' => '9',
        'DEFAULT_PATH' => '/var/www/programming-challenges/storage/app/public/attendees/',
        'DEFAULT_LOOP_ERROR' => 'Cannot compile your source code... Maybe you have an infinite loop ?',
        'DEFAULT_PRESENTATION_ERROR' => 'Presentation error'
    ]
];