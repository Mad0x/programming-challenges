<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Programming-challenges</title>

    <!-- Styles -->
    <link href="{{ asset('css/codemirror.css') }}" rel="stylesheet">
    <link href="{{ asset('css/materialize.css') }}" rel="stylesheet">

    <!-- Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/default.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/zf/dt-1.10.16/datatables.min.css"/>
    <style>
        body {
            display: flex;
            min-height: 100vh;
            flex-direction: column;
        }

        main {
            flex: 1 0 auto;
        }
    </style>
</head>
<body>
<main>
    <div id="app">
        {{--<nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>--}}

        <nav>

            <div class="nav-wrapper blue darken-3">
                <a href="{{route('home')}}" style="text-decoration: none" class="brand-logo">Programming-challenges</a>
                <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                    @guest
                        <li><a href="{{route('login')}}" style="text-decoration: none">Login</a></li>
                        <li><a href="{{route('register')}}" style="text-decoration: none">Register</a></li>
                        @else
                            <li>Welcome {{Auth::User()->nickname}}</li>
                            <li><a href="{{route('home')}}" style="text-decoration: none">Dashboard</a></li>
                            <li><a href="{{route('challenges')}}" style="text-decoration: none">Challenges</a></li>

                            @if(Auth::User()->level == 1)
                                <li><a href="{{route('admin')}}" style="text-decoration: none">Admin</a></li>
                            @endif


                            <li>
                                <form id="logout-form" action="{{route('logout')}}" method="POST" style="display:none;">
                                    {{ csrf_field() }}
                                </form>
                                <a href="#" onclick="$('#logout-form').submit();">Logout</a>
                            </li>
                            @endguest
                </ul>
                <ul class="side-nav" id="mobile-demo">
                    @guest
                        <li><a href="{{route('login')}}" style="text-decoration: none">Login</a></li>
                        <li><a href="{{route('register')}}" style="text-decoration: none">Register</a></li>
                        @else
                            <li>Welcome {{Auth::User()->nickname}}</li>
                            <li><a href="{{route('home')}}" style="text-decoration: none">Dashboard</a></li>
                            <li><a href="{{route('challenges')}}" style="text-decoration: none">Challenges</a></li>

                            @if(Auth::User()->level == 1)
                                <li><a href="{{route('admin')}}" style="text-decoration: none">Admin</a></li>
                            @endif


                            <li>
                                <form id="logout-form" action="{{route('logout')}}" method="POST" style="display:none;">
                                    {{ csrf_field() }}
                                </form>
                                <a href="#" onclick="$('#logout-form').submit();">Logout</a>
                            </li>
                            @endguest
                </ul>
            </div>
        </nav>
        @yield('content')

    </div>
</main>
<footer class="page-footer blue darken-3">
    <div class="container">
        <div class="row">
            <div class="col l4 s12">
                <h5 class="white-text">Created with passion</h5>
                <p class="grey-text text-lighten-4">Powered by Laravel, Materialize and JQuery</p>
            </div>
            <div class="col l6 offset-l2 s12">
                <h5 class="white-text">Navigation</h5>
                <nav style="box-shadow: none;">
                    <div class="nav-wrapper blue darken-3">
                        <div class="col s12">
                            @yield('breadcrumb')
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            Copyright &copy; 2017 - Micaël Cid, Jeff Muraro, Daniela Carvalhal
        </div>
    </div>
</footer>


<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/materialize.js') }}"></script>
<script src="{{ asset('js/codemirror.js') }}"></script>
<script src="{{ asset('js/clike.js') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/zf/dt-1.10.16/datatables.min.js"></script>
<script>
    $(".button-collapse").sideNav();
</script>
@yield('script')
</body>
</html>
