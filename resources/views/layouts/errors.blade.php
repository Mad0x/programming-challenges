@if ($errors->any())
    <ul class="collection red darken-1">
        @foreach ($errors->all() as $error)
            <li class="collection-item red darken-1">{{ $error }}</li>
        @endforeach
    </ul>
@endif