<tr>
    <td>{{$challenge->name}}</td>
    <td>
        <i class="material-icons" style="color: #0D47A1">
            @for($i = 0; $i < $challenge->difficulty; $i++)
                star
            @endfor
        </i>
    </td>
    <td>
        <a href="{{ route('editChallenge', ['id'=>$challenge->id]) }}" class="waves-effect waves-light blue darken-2 btn">Edit</a>
        <a class="waves-effect waves-light btn modal-trigger blue darken-2" href="#modal{{$challenge->id}}">Delete</a>
    </td>
</tr>

<div id="modal{{$challenge->id}}" class="modal" >
    <div class="modal-content">
        <h4>Delete confirmation</h4>
        <p>Are you sure you want to delete the challenge named "{{$challenge->name}}"</p>
    </div>
    <div class="modal-footer">
        <a href="#" class="modal-action modal-close waves-effect waves-green btn-flat">Disagree</a>
        <a href="{{route('delete', ['id'=>$challenge->id])}}" class="modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
    </div>
</div>
