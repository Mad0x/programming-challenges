<tr>
    <td>{{$challenge->name}}</td>
    @if($challenge->getOriginal('pivot_status') == "IN PROGRESS")
        <td class="red-text text-darken-2">{{$challenge->getOriginal('pivot_status')}}</td>
        <td class="red-text text-darken-2">Not executed</td>
    @else
        <td class="green-text text-darken-2">{{$challenge->getOriginal('pivot_status')}}</td>
        <td class="green-text text-darken-2">{{$challenge->getOriginal('pivot_executionTime')}}
            [s]
        </td>
    @endif
    <td><a href="{{route('challenge',$challenge->id)}}"
           class="waves-effect waves-light btn blue darken-3">Watch
            challenge</a></td>
</tr>