<li>
    <div class="collapsible-header blue darken-3">
        <span class="white-text">{{$challenge->name}}</span>
        <i class="material-icons right" style="color: yellow">
            @for($i = 0; $i < $challenge->difficulty; $i++)
                star
            @endfor
        </i>
    </div>
    <div class="collapsible-body">
        <span>{{$challenge->description}}</span>
        <p class="right-align">
            <a href="{{route('challenge',$challenge->id)}}"
               class="waves-effect waves-light blue darken-2 btn">Voir le challenge</a>
        </p>
    </div>
</li>