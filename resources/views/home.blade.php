@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col s12">
                </br>
                @if(Session::has('success'))
                    <div class="card-panel green darken-1">
                        {{ Session::get('success') }}
                    </div>
                @endif
                <ul class="collapsible" data-collapsible="accordion">
                    <li>
                        <div class="collapsible-header active"><i class="material-icons">mode_edit</i>Edit your profile</div>
                        <div class="collapsible-body">
                            <form method="POST" action="{{route('save')}}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="nickname" name="nickname" type="text"
                                               class="validate {{ $errors->has('nickname') ? ' has-error' : '' }}"
                                               value="{{  Auth::user()->nickname }}"
                                               required>
                                        <label for="nickname" data-error="Veuillez renseigner un pseudo">Pseudo</label>

                                        @if ($errors->has('nickname'))
                                            <div class="card-panel red darken-1">{{ $errors->first('nickname') }}</div>
                                        @endif
                                    </div>

                                    <div class="input-field col s6">
                                        <input id="mail" name="email" type="email"
                                               class="validate {{ $errors->has('email') ? ' has-error' : '' }}"
                                               value="{{ Auth::user()->email }}" required>
                                        <label for="mail">E-mail</label>

                                        @if ($errors->has('email'))
                                            <div class="card-panel red darken-1">{{ $errors->first('email') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="password" name="password" type="password"
                                               class="validate {{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password"
                                               data-error="Veuillez renseigner un mot de passe">Password</label>

                                        @if ($errors->has('password'))
                                            <div class="card-panel red darken-1">{{ $errors->first('password') }}</div>
                                        @endif
                                    </div>

                                    <div class="input-field col s6">
                                        <input id="password_confirmation" name="password_confirmation" type="password"
                                               class="validate {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <label for="password_confirmation"
                                               data-error="Veuillez renseigner une deuxième fois le mot de passe">Password
                                            confirmation</label>

                                        @if ($errors->has('password_confirmation'))
                                            <div class="card-panel red darken-1">{{ $errors->first('password_confirmation') }}</div>
                                        @endif
                                    </div>
                                </div>

                                <button class="btn waves-effect waves-light blue darken-3" type="submit" name="action">
                                    Edit
                                </button>
                            </form>
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header"><i class="material-icons">code</i>Challenges</div>
                        <div class="collapsible-body">
                            @if($challenges->isEmpty())
                                <div class="card-panel red darken-1 white-text">You haven't participated to a challenge
                                    yet. Click <a href="{{route('challenges')}}">here</a> to check the list of challenges available !
                                </div>
                            @else
                                <table>
                                    <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Status</th>
                                        <th>Execution time</th>
                                        <th>#</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @each('layouts.user_challenges',$challenges,'challenge')
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection
@section('breadcrumb')
    <a href="{{route('home')}}" class="breadcrumb blue darken-3">Dashboard</a>
@endsection
