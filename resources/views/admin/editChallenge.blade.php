@extends('layouts.app')

@section('content')
    <div class="container">
        </br>
        <div class="row">
            <form class="col s8 offset-s2" method="POST" action="{{ route('saveChallenge', ['id'=>$challenge->id]) }}">
                {{ csrf_field() }}
                <div class="row">

                    <div class="input-field col s12">
                        <h5>Description</h5>

                        <textarea id="description" name="description" class="materialize-textarea">{{ $challenge->description }}</textarea>

                        @if ($errors->has('description'))
                            <div class="card-panel red darken-1">{{ $errors->first('description') }}</div>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <h5>Unit test</h5>

                        <textarea id="unitTest" name="unitTest" class="materialize-textarea">{{ $challenge->testDescription }}</textarea>

                        @if ($errors->has('unitTest'))
                            <div class="card-panel red darken-1">{{ $errors->first('unitTest') }}</div>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s6">
                        <h5>Your desired output</h5>

                        <textarea name="output" id="output" class="materialize-textarea"></textarea>

                        @if ($errors->has('code'))
                            <div class="card-panel red darken-1">{{ $errors->first('code') }}</div>
                            </span>
                        @endif
                    </div>
                    <div class="input-field col s6">
                        <h5>Difficulty</h5>

                        <p class="range-field">
                            <input  type="range" id="difficulty" name="difficulty" value="{{ $challenge->difficulty }}" min="1" max="10" />
                        </p>
                        @if ($errors->has('difficulty'))
                            <div class="card-panel red darken-1">{{ $errors->first('difficulty') }}</div>
                        @endif
                    </div>
                </div>
                <input type="hidden" name="id" value="{{ $challenge->id }}"/>
                <button class="btn waves-effect waves-light blue darken-3" type="submit" name="action">Edit</button>
            </form>
        </div>
    </div>
@endsection
@section('breadcrumb')
    <a href="{{route('home')}}" class="breadcrumb blue darken-3">Index</a>
    <a href="{{route('admin')}}" class="breadcrumb blue darken-3">Admin</a>
    <a href="{{route('editChallenge', ['id'=>$challenge->id])}}" class="breadcrumb blue darken-3">Edit</a>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('.materialize-textarea').trigger('autoresize');
        });
    </script>
@endsection