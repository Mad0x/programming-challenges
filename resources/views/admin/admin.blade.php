@extends('layouts.app')

@section('content')
    <div class="container">
        </br>
        <div class="row">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header active"><i class="material-icons">file_upload</i>Modify challenge
                    </div>
                    <div class="collapsible-body">

                        @if(session('success'))
                            <div class="card-panel green darken-1">{!! session('success') !!}</div>
                        @endif

                        <table id="myTable">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Difficulty</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @each('layouts.challengesAdmin',$challenges,'challenge')
                            </tbody>
                        </table>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header active"><i class="material-icons">mode_edit</i>Edit user</div>
                </li>
            </ul>
        </div>
    </div>
@endsection
@section('breadcrumb')
    <a href="{{route('home')}}" class="breadcrumb blue darken-3">Index</a>
    <a href="{{route('admin')}}" class="breadcrumb blue darken-3">Admin</a>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            // the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
            $('.modal').modal();
            $('#myTable').DataTable(({
                "bInfo": false,
            }));
        });
    </script>
@endsection
