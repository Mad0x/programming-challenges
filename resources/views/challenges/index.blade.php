@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="row">
                    <div class="col s12 m12">
                        </br>
                        <ul class="collapsible" data-collapsible="accordion">
                            @each('layouts.challenges',$challenges,'challenge')
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('breadcrumb')
    <a href="{{route('home')}}" class="breadcrumb blue darken-3">Dashboard</a>
    <a href="{{route('challenges')}}" class="breadcrumb blue darken-3">Challenges list</a>
@endsection