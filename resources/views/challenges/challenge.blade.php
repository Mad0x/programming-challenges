@extends('layouts.app')

@section('content')
    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="row">
                    @if(Session::has('error'))
                        <div class="card-panel red darken-1">
                            <span class="white-text">{{ Session::get('error')}}</span>
                        </div>
                    @endif
                    @if(Session::has('success'))
                        <div class="card-panel green darken-1">
                            <span class="white-text">{{ Session::get('success') }}</span>
                        </div>
                    @endif
                    <div class="col s12 m12">
                        @if ($errors->has('code'))
                            <div class="card-panel red darken-1">{{ $errors->first('code') }}</div>
                        @endif
                        @if ($errors->has('file'))
                            <div class="card-panel red darken-1">{{ $errors->first('file') }}</div>
                        @endif
                        <div class="card">

                            <form class="col s12" action="{{route('compile')}}" method="post"
                                  enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input type="hidden" value="{{$challenge->id}}" name="id">
                                <div class="card-content black-text">
                                    <span class="card-title">{{$challenge->name}}</span>
                                    <h5>Description</h5>
                                    <p> {!! nl2br(e($challenge->description)) !!}</p>

                                    <h5>Unit test</h5>
                                    <p> {!! nl2br(e($challenge->testDescription)) !!}</p>

                                </div>
                                <div class="card-action">
                                    <h5>Submit your code here</h5>
                                    <div class="row">
                                        <div class="input-field col s12">
                                                <textarea name="code" id="code"
                                                          class="materialize-textarea"></textarea>
                                            <div class="file-field input-field">
                                                <div class="btn blue darken-2">
                                                    <span>File</span>
                                                    <input type="file" name="file">
                                                </div>
                                                <div class="file-path-wrapper">
                                                    <input class="file-path validate" type="text">
                                                </div>
                                            </div>
                                            <button type="submit"
                                                    class="btn btn-large blue darken-2 waves-effect waves-light right">
                                                Send
                                                my
                                                code
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('breadcrumb')
    <a href="{{route('home')}}" class="breadcrumb blue darken-3">Dashboard</a>
    <a href="{{route('challenges')}}" class="breadcrumb blue darken-3">Challenges list</a>
    <a href="{{route('challenge',$challenge->id)}}" class="breadcrumb blue darken-3">{{$challenge->name}}</a>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            CodeMirror.fromTextArea(
                document.getElementById("code"),
                {lineNumbers: true, matchBrackets: true, mode: "text/x-csharp"}
            );
        });
    </script>
@endsection