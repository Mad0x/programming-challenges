@extends('layouts.app')

@section('content')
    <div class="container">
        </br>
        <div class="row">
            <form class="col s8 offset-s2" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <div class="row">
                    <div class="input-field col s12">
                        <input id="email" name="email" type="email"
                               class="validate"
                               value="{{ old('email') }}" required>
                        <label for="email" data-error="Please give an e-mail">E-mail</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="password" name="password" type="password"
                               class="validate" required>
                        <label for="password" data-error="Please give a password">Password</label>
                    </div>
                </div>
                @include('layouts.errors')
                <div class="row">
                    <div class="col s8">
                        <div class="left">
                            <button class="btn waves-effect waves-light blue darken-3" type="submit" name="action">
                                Login
                            </button>
                            <a href="{{route('password.request')}}" class="waves-effect waves-light btn blue darken-3">Reset
                                password</a>
                        </div>
                    </div>
                    <div class="col s4">
                        <div class="right">
                            <input type="checkbox" id="test5"/>
                            <label for="test5">Remember me ?</label>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection
@section('breadcrumb')
    <a href="{{route('home')}}" class="breadcrumb blue darken-3">Index</a>
    <a href="{{route('login')}}" class="breadcrumb blue darken-3">Login</a>
@endsection