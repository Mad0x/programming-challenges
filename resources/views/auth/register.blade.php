@extends('layouts.app')

@section('content')
    <div class="container">
        </br>
        <div class="row">
            <form class="col s8 offset-s2" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}

                <div class="row">
                    <div class="input-field col s12">
                        <input id="nickname" name="nickname" type="text"
                               class="validate {{ $errors->has('nickname') ? ' has-error' : '' }}"
                               value="{{ old('nickname') }}"
                               required>
                        <label for="nickname" data-error="Please give a pseudo">Pseudo</label>

                        @if ($errors->has('nickname'))
                            <div class="card-panel red darken-1">{{ $errors->first('nickname') }}</div>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="mail" name="email" type="email"
                               class="validate {{ $errors->has('email') ? ' has-error' : '' }}"
                               value="{{ old('email') }}" required>
                        <label for="mail" data-error="Please give an e-mail">E-mail</label>

                        @if ($errors->has('email'))
                            <div class="card-panel red darken-1">{{ $errors->first('email') }}</div>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="password" name="password" type="password"
                               class="validate {{ $errors->has('password') ? ' has-error' : '' }}" required>
                        <label for="password" data-error="Please give a password">Password</label>

                        @if ($errors->has('password'))
                            <div class="card-panel red darken-1">{{ $errors->first('password') }}</div>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="password_confirmation" name="password_confirmation" type="password"
                               class="validate {{ $errors->has('password_confirmation') ? ' has-error' : '' }}"
                               required>
                        <label for="password_confirmation"
                               data-error="Please give a second time a password for the confirmation">Password
                            confirmation</label>

                        @if ($errors->has('password_confirmation'))
                            <div class="card-panel red darken-1">{{ $errors->first('password_confirmation') }}</div>
                            </span>
                        @endif
                    </div>
                </div>
                <button class="btn waves-effect waves-light blue darken-3" type="submit" name="action">Register</button>
            </form>
        </div>
    </div>
@endsection
@section('breadcrumb')
    <a href="{{route('home')}}" class="breadcrumb blue darken-3">Index</a>
    <a href="{{route('register')}}" class="breadcrumb blue darken-3">Register</a>
@endsection
