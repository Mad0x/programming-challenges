@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <form class="col s8 offset-s2">
            <div class="row">
                <div class="input-field col s12">
                    <input id="mail" type="email" class="validate" required>
                    <label for="mail" data-error="Veuillez renseigner un e-mail">E-mail</label>
                </div>
            </div>
            <button class="btn waves-effect waves-light blue darken-3" type="submit" name="action">
                Send Password Reset Link
            </button>
        </form>
    </div>
</div>
@endsection
